import socket
import select
import sys
import data
import hashlib


PASSWORD = hashlib.md5(b'led zeppelin is better').digest()
ADDRESS = ('0.0.0.0', 9095)
OPTION_1, OPTION_2, OPTION_3, OPTION_4, OPTION_5, OPTION_6, OPTION_7 = '1 2 3 4 5 6 7'.split()
KEY_ERROR_MSG = 'Can\'t found data, invalid input!'
SPLIT_BY = '$$$'

options = {
    OPTION_1: data.albums_name,
    OPTION_2: data.list_of_songs_in_album,
    OPTION_3: data.length_of_song,
    OPTION_4: data.song_words,
    OPTION_5: data.album_name_by_song,
    OPTION_6: data.get_song_by_word_in_title,
    OPTION_7: data.get_song_by_word
}


def get_data(params):
    """
    The function get the request, and get the response from the data file according to the request

    :param params: the parameters, type: list
    :return: the answer, type: string
    """
    command, arguments = params[0], params[1]
    try:
        return options[command](arguments)
    except KeyError:
        return KEY_ERROR_MSG


def create_listen_socket():
    """
    The function create and return a listen socket

    :return: listen socket, type: socket
    """
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        server_socket.bind(ADDRESS)
        server_socket.listen()
    except Exception as e:
        server_socket.close()
        sys.exit(f"\n{e}\n\nCannot connect to the port, Try to run the server script again!")

    return server_socket


def server():
    """
    The function create, setting up and mange a server

    :return: None
    """
    # client_sockets - used to save all the connected sockets
    # is_log_in - used to check if the user logged in (a list of True/ False map by client_sockets
    client_sockets = []
    is_log_in = []

    data.init_data_base()

    server_socket = create_listen_socket()
    print('Server listening to new connection...\n')

    while True:
        ready_to_read, ready_to_write, in_error = select.select([server_socket] + client_sockets, [], [])

        for sock in ready_to_read:

            if sock is server_socket:
                connection, address = sock.accept()

                # adding the connection - login -> False
                client_sockets.append(connection)
                is_log_in.append(False)

                connection.sendall("Welcome to pink Floyd api\n".encode())
                print(f"[*] new connection: {address}\n[*]Total connected: {len(is_log_in)}\n")
                continue

            try:
                request = sock.recv(1024).decode()

            except Exception:
                # remove socket from client_sockets and is_logged_in
                is_log_in.pop(client_sockets.index(sock))
                client_sockets.remove(sock)
                print("-- client leave")
                continue

            # if the request from the current client is empty, he might not leave (only with "Quit" message) so we just continue
            if not request:
                continue

            # remove socket from client_sockets and is_logged_in
            if request.endswith('Quit'):
                is_log_in.pop(client_sockets.index(sock))
                client_sockets.remove(sock)
                print("-- client leave")
                continue

            #  if the current client not logged in we check if he send the password
            if not is_log_in[client_sockets.index(sock)]:
                index: int = client_sockets.index(sock)
                answer: bool = hashlib.md5(request.encode()).digest() == PASSWORD
                sock.send(str(answer).encode())
                is_log_in[index] = answer
                continue

            # just sent a response
            parameters = request.split(SPLIT_BY)
            sock.send(get_data(parameters).encode())

    server_socket.close()


def main():
    server()


if __name__ == "__main__":
    main()
