import socket
import sys


ADDRESS = ('127.0.0.1', 9095)
VALID_OPTION = '1234567'
MENU = '''
Welcome to the Pink Floyd api, please choose option:
1 - get name of all albums
2 - List of all songs in album
3 - Length of song
4 - Get song words
5 - Get album name by song
6 - Get song by some name in the title
7 - Get song by name of some word that contains this word
Quit - Exit
'''


def get_message(sock):
    """
    The function get socket, and try to received data, if some problem occur, the function return error message

    :param sock: a socket connection to get data, type: socket.pyi
    :return: the message
    """
    try:
        return sock.recv(8192).decode()
    except Exception:
        sock.close()
        sys.exit('\nError: The server shut down!')


def send_message(sock, data):
    """
    The function get a socket, and try to send the data

    :param sock: a socket connection, socket.pyi
    :param data: data to sent, type str
    :return: None
    """
    try:
        sock.sendall(data.encode())
    except Exception:
        sock.close()
        sys.exit('\nError: The server shut down!')


def get_option():
    """
    The function get option from user (not try except because it string), and return the user choice
    code-note: in while condition, len 1 -> to check if the user is 45, to valid numbers, but i only need one...
    No try except on purpose
    :return: user choice, type: str
    """

    option = input("Please choose option: ")
    while not(option in VALID_OPTION and len(option) == 1) and option != 'Quit':
        option = input("Invalid option, try again: ")
    return option


def get_option_data(option):
    """
    The function get option, and ask the user for data for the current option
    No try except on purpose
    :param option: user option, type: str
    :return: The user answer, type: str
    """
    if option == '2':
        return input('\nEnter album name: ')
    if option in '345':
        return input('Enter song name: ')
    if option == '6':
        return input('\nEnter word to search: ').lower()
    if option == '7':
        return input('\nEnter word to search in the lyrics: ').lower()
    return 'null'


def login(sock):
    """
    The function get sock, ask him for a password, and check if he pass it

    :param sock: a socket connection, type: socket.pyi
    :return: True -> pass, else -> False, type: bool
    """
    while True:
        try:
            password = input('Enter password: ')
        except KeyboardInterrupt:
            send_message(sock, 'Quit')
            sock.close()

            sys.exit('Bye Bye')

        if len(password) == 0:
            continue
        send_message(sock, password)
        if get_message(sock) == 'True':
            break


def client_connection():
    """
    The function get socket and mange the request of the user

    :return: None
    """
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        client.connect(ADDRESS)
        print(client.recv(64).decode(), end='\n\n')
        login(client)
    except ConnectionRefusedError as e:
        sys.exit(f'Cant connect to the server\n\n{e}')

    while True:
        print(MENU)
        try:
            option = get_option()
            option_data = get_option_data(option)
        except KeyboardInterrupt:
            send_message(client, 'Quit')
            client.close()
            sys.exit('\n\nBye Bye!')

        send_message(client, f'{option}$$${option_data}')

        if option == 'Quit':
            client.close()
            sys.exit('\nBye Bye!')

        response = get_message(client)
        print(f"\nServer answer:\n{response}\n")


def main():
    client_connection()


if __name__ == "__main__":
    main()
