SPLIT_ALBUMS = '#'
SPLIT_SONGS = '*'
SPLIT_DATA = '::'

ALBUM_INDEX = 0
CREATOR_INDEX = 0
LEN_INDEX = 1
WORDS_INDEX = 2

MODE = 'r'
FILE_PATH = 'Pink_Floyd_DB.txt'

# note: keyError -> check it in the server.py !


def albums_name(a: str) -> str:
    """
    function return strings of all albums

    :param a: don't used, take it because in server get_data function, any function take one argument except this
    so this take argument to work with the other function
    :return: a string of albums
    """
    return '\n'.join(list(dataBase_albums_to_songs.keys()))


def list_of_songs_in_album(album: str) -> str:
    """
    The function take album, and return all the song in the album

    :param album: album - to know from were to take songs
    :return: a string of songs
    """
    return '\n'.join(list(dataBase_albums_to_songs[album]))


def length_of_song(song: str) -> str:
    """
    The function get song, and return the length of the song

    :param song: a song - to get the length of the song
    :return: length of the song
    """
    return f'\nLength of song {song}: {dataBase_data_in_song[song][LEN_INDEX]}'


def song_words(song: str) -> str:
    """
    The function get song, and return the words in the song

    :param song: a song - to get the words of the song
    :return: the words of the song
    """
    return f"\nWords of {song}:\n{dataBase_data_in_song[song][WORDS_INDEX]}"


def album_name_by_song(song_to_search: str) -> str:
    """
    The function get song and return the name of the album of the song

    :param song_to_search:
    :return: the album name
    """
    for album, songs in dataBase_albums_to_songs.items():
        if song_to_search in songs:
            return album


def get_song_by_word_in_title(word: str) -> str:
    """
    The function get a word, and return all the songs names that contains the word

    :param word: a word to search
    :return: a string of all the songs
    """
    answer = []
    for album, songs in dataBase_albums_to_songs.items():
        for song in songs:
            if word.lower() in song.lower():
                answer.append(song)
    return '\n'.join(answer)


def get_song_by_word(word: str) -> str:
    """
    The function get a word, and return all the songs the contains the word in the song-words

    :param word: a word to search
    :return: a string of all the songs
    """
    answer = []
    for songs in dataBase_albums_to_songs.values():
        for song in songs:
            if word.lower() in dataBase_data_in_song[song][WORDS_INDEX].lower():
                answer.append(song)
    return '\n'.join(answer)


def get_data(path, mode):
    """
    The function get path and mode, and return reader file

    :param path: the path to the file, type: str
    :param mode: the mode to open the file, type: str
    :return: the text of the file
    """
    # does not create try except if the file don't exist, because in the word, they say that the file exist
    return open(path, mode).read()


def init_data_base():
    """
    function init 2 data base
    1: key - album, value - list of songs
    2: key - song name, value - [creator, length, words]

    :return: None
    """
    global dataBase_albums_to_songs, dataBase_data_in_song

    dataBase_albums_to_songs, dataBase_data_in_song = {}, {}

    data = get_data(FILE_PATH, MODE)

    for album in data.split(SPLIT_ALBUMS)[1:]:
        # init a key -> album name, and value -> empty list
        album_name = album.split(SPLIT_DATA)[ALBUM_INDEX]
        dataBase_albums_to_songs[album_name] = []

        # in any album key - list of all songs (first dict)
        for song in album.split(SPLIT_SONGS)[1:]:
            dataBase_albums_to_songs[album_name].append(song.split(SPLIT_DATA)[0])

        # in any song, list of data -> [creator, length, words] (second dict)
        for song in album.split(SPLIT_SONGS)[1:]:
            name, creator, length, words = song.split(SPLIT_DATA)
            dataBase_data_in_song[name] = [creator, length, words]
